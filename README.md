# Frases Feas

This is a library that display random phrases that used to be used on internet and social media.

## Installation
use the node package manager [npm](https://www.npmjs.com/) to install this dependency.

```bash
npm install -g frases-feas
```

## Usage
```bash
> frases-feas
```

### or

```javascript
const phrase = require('frases-feas');
phrase();
```

## License
[MIT](https://choosealicense.com/licenses/mit/)